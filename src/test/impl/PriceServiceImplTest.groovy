package com.knabu.service.impl

import com.knabu.service.PriceService
import spock.lang.Specification

import static org.knowm.xchange.currency.Currency.BTC
import static org.knowm.xchange.currency.Currency.USD

class PriceServiceImplTest extends Specification {
    def "test getBitstamp"() {
        given:
        PriceService priceService = new PriceServiceImpl()

        when:
        def price = priceService.getBitstamp(USD, BTC)
        println price

        then:
        price > 100
    }

    def "test getDiffs"() {
        given:
        PriceService priceService = new PriceServiceImpl()

        when:
        BigDecimal diff = priceService.getDiffs()
        println diff

        then:
        diff > 1.000000000
    }
}
