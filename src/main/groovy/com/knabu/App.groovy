package com.knabu

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
//@EnableHystrix
//@EnableHystrixDashboard
//@EnableCircuitBreaker
class App {

    static void main(String[] args) {
        SpringApplication.run(App, args)
    }

}
