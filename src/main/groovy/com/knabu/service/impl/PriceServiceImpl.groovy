package com.knabu.service.impl

import com.knabu.service.PriceService
import groovy.json.JsonSlurper
import groovy.util.logging.Slf4j
import org.knowm.xchange.Exchange
import org.knowm.xchange.ExchangeFactory
import org.knowm.xchange.bitfinex.v1.BitfinexExchange
import org.knowm.xchange.bitstamp.BitstampExchange
import org.knowm.xchange.coinbase.CoinbaseExchange
import org.knowm.xchange.coinfloor.CoinfloorExchange
import org.openexchangerates.oerjava.OpenExchangeRates
import org.springframework.stereotype.Service

import static org.knowm.xchange.currency.CurrencyPair.BTC_GBP

@Slf4j
@Service
class PriceServiceImpl implements PriceService {


    public static final String OER_ID = "b8234bb177564c7b9dbf34298b235b6e"
    public static final String TANJALO = 'https://tanjalo.com/v1/spot'
    public static final String LUNO = 'https://api.mybitx.com/api/1/ticker?pair='

    @Override
    BigInteger getDiffs() {

        BigDecimal NgnUsdRate = new OpenExchangeRates(OER_ID).currency('NGN')
        BigDecimal GbpUsdRate = new OpenExchangeRates(OER_ID).currency('GBP')
        BigDecimal GbpNgnRate = NgnUsdRate / GbpUsdRate

//get GBP price from coinfloor

        Map<String,Exchange> exchangeList = new HashMap<>()
        Map buyList= new HashMap<>()
        Map sellList= new HashMap<>()

//        exchangeList.put('bitstamp', ExchangeFactory.INSTANCE.createExchange(BitstampExchange.name))
        exchangeList.put('bitfinex', ExchangeFactory.INSTANCE.createExchange(BitfinexExchange.name))
        exchangeList.put('coinbase', ExchangeFactory.INSTANCE.createExchange(CoinbaseExchange.name))
        exchangeList.put('coinfloor', ExchangeFactory.INSTANCE.createExchange(CoinfloorExchange.name))

        exchangeList.each {
            try {
                buyList.put(it.key,it.value.getMarketDataService().getTicker(BTC_GBP).last as BigDecimal)
            } catch (e) {
                println e.message
            }
        }

        def bestAsk = buyList.sort({it.value}).entrySet().first()

        sellList.put('tanjaloBid',new JsonSlurper().parse(new URL(TANJALO)).bid[0].value as BigDecimal)
        sellList.put('lunoBid ', new JsonSlurper().parse(new URL(LUNO + 'XBTNGN')).bid as BigDecimal)

        def bestBid = sellList.sort({it.value}).entrySet().last()

        (bestBid.value / GbpNgnRate / bestAsk.value) * 100
    }


//    def tanjaloBid = new JsonSlurper().parse(new URL(TANJALO)).bid[0].value as BigDecimal
//    def lunoBid = new JsonSlurper().parse(new URL(LUNO + 'XBTNGN')).bid as BigDecimal
    ////        def lunoBid  =  new JsonSlurper().parse(new URL(LUNO + 'XBTNGN')).ask  as BigDecimal
////        def tanjaloBid =  new JsonSlurper().parse(new URL(TANJALO)).offer[0].value as BigDecimal
//
////        BigDecimal gbpBtcAsk = coinfloor.getMarketDataService().getTicker(BTC_GBP).ask;
////        BigDecimal gbpBtcAsk = bitfinex.getMarketDataService().getTicker(BTC_GBP).ask;
////        BigDecimal gbpBtcAsk = coinbase.getMarketDataService().getTicker(BTC_GBP).ask; //todo doesn't work, quotes in USD
//        BigDecimal gbpBtcAsk = exchangeList.get('coinbase').getMarketDataService().getTicker(BTC_GBP).last
////        BigDecimal ngnBtc = luno.getMarketDataService().getTicker(CurrencyPair(BTC,NGN)).last;
//
//// get Base / NAI rate
//
//
////        get rate in GBP of the NGN price
//
//        BigDecimal tanjaloBidInGbp = tanjaloBid / GbpNgnRate
//        BigDecimal lunoBidInGbp = lunoBid / GbpNgnRate
//
////        BigDecimal diffPercent = tanjaloBidInGbp / gbpBtcAsk
//        BigDecimal diffPercent = lunoBidInGbp / gbpBtcAsk
//

}
