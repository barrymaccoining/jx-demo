package com.knabu.endpoint


import com.knabu.service.PriceService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(produces = 'application/json')
@Api(value = "/eth/", description = "Operations with ethereum")
class ArbController {

    @Autowired
    private PriceService service

    @ApiOperation(
            value = "get arb rate", notes = "no notes")
    @GetMapping(value = "/arb/rate")
    BigDecimal getRate() {
        service.diffs as BigDecimal
    }

}
