package com.knabu.service

interface PriceService {
    BigInteger getDiffs()
}
