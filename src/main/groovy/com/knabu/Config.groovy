package com.knabu

import com.google.common.base.Predicate
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.client.RestTemplate
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.ApiInfo
import springfox.documentation.service.Contact
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2

import static com.google.common.base.Predicates.and
import static com.google.common.base.Predicates.not
import static springfox.documentation.builders.PathSelectors.regex

@Configuration
//@EnableSwagger2
class Config {

//    @Value('${termsOfServiceUrl}')
//    private String termsOfServiceUrl
//    @Value('${contactName}')
//    private String contactName
//    @Value('${contactEmail}')
//    private String contactEmail
//    @Value('${contactUrl}')
//    private String contactUrl

    static volatile boolean httpServiceRunning = true

//    @Bean
//    Docket ethApi() {
//        return new Docket(DocumentationType.SWAGGER_2)
//                .groupName("arb-api")
//                .apiInfo(apiInfo())
//                .select()
//                .apis(RequestHandlerSelectors.basePackage("com.knabu"))
//                .build()
//    }

//    @Bean
//    RestTemplate restTemplate() {
//        new RestTemplate()
//    }

//    private Predicate<String> sysApiPaths() {
//        and(
//                regex("/.*"),
//                not(regex("/error/*")),
//                not(regex("/hystrix/*"))
//        )
//    }
//
//    private ApiInfo apiInfo() {
//        return new ApiInfoBuilder()
//                .title("Arb API")
//                .description("Api arb ops")
//                .termsOfServiceUrl(termsOfServiceUrl)
//                .contact(new Contact(contactName, contactUrl, contactEmail))
//                .version("1.0")
//                .build();
//    }

}
